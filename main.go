package main

import (
	"context"
	"embed"
	"fmt"
	"io/fs"
	"log"
	"os"
	"path/filepath"
	"strings"

	"codeberg.org/gonix/gio/pipe"
	"codeberg.org/gonix/gio/unix"
	"github.com/tetratelabs/wazero"
	"github.com/tetratelabs/wazero/imports/wasi_snapshot_preview1"
	"github.com/tetratelabs/wazero/sys"
)

// catFS is an embedded filesystem limited to test.txt
//
//go:embed testdata/test.txt
var catFS embed.FS

//go:embed testdata/*.wasm
var wasmFiles embed.FS

type Wasi struct {
	name    string
	runtime wazero.Runtime
	config  wazero.ModuleConfig
	module  wazero.CompiledModule
	args    []string
}

func (w Wasi) Run(ctx context.Context, stdio unix.Stdio) error {
	if w.runtime == nil {
		return fmt.Errorf("nil wazero runtime")
	}
	if w.config == nil {
		w.config = wazero.NewModuleConfig()
	}
	if w.name == "" {
		w.name = "wasi"
	}

	// Combine the above into our baseline config, overriding defaults.
	config := w.config.
		// By default, I/O streams are discarded and there's no file system.
		WithStdin(stdio.Stdin()).WithStdout(stdio.Stdout()).WithStderr(stdio.Stderr())

	args := append([]string{w.name}, w.args...)
	if _, err := w.runtime.InstantiateModule(ctx, w.module, config.WithArgs(args...)); err != nil {
		if exitErr, ok := err.(*sys.ExitError); ok && exitErr.ExitCode() != 0 {
			return pipe.Error{Code: int(exitErr.ExitCode()), Err: exitErr}
		} else if !ok {
			return pipe.Error{Code: pipe.UnknownError, Err: fmt.Errorf("wasi module error: %w", err)}
		}
	}
	return nil
}

// WithArgs adds the CLI args to started wasi code
func (w Wasi) WithArgs(args ...string) Wasi {
	w.args = args
	return w
}

type NameMapperFunc func(path string) string

// NameMapper maps a wasm file path to module NameMapper
// eg testdata/sbase/cat.wasm -> sbase/cat
// or testdata/9base/wc.wasm -> wc
type NameMapper interface {
	Name(path string) string
}

type NoMapper struct{}

func (NoMapper) Name(path string) string { return path }

type funcMapper struct {
	f NameMapperFunc
}

func (f funcMapper) Name(path string) string { return f.f(path) }

// WasiBuilder ties the runtime and a top level module config into code can
// emit the Wasi instances this helps sharing the top-level wazero objects with
// Wasi structs, which implements the gio/unix.Pipe interface
type WasiBuilder struct {
	runtime wazero.Runtime
	config  wazero.ModuleConfig
	mapper  NameMapper
}

func NewWasiBuilder(runtime wazero.Runtime, config wazero.ModuleConfig) WasiBuilder {
	return WasiBuilder{runtime: runtime, config: config, mapper: NoMapper{}}
}

func (b WasiBuilder) Build(name string, module wazero.CompiledModule) Wasi {
	return Wasi{runtime: b.runtime, config: b.config, name: name, module: module}
}

func (b WasiBuilder) WithMapper(mapper NameMapper) WasiBuilder {
	b.mapper = mapper
	return b
}

func (b WasiBuilder) WithMapperFunc(mapperFunc NameMapperFunc) WasiBuilder {
	b.mapper = funcMapper{f: mapperFunc}
	return b
}

// CompileModules traverses the emed.FS, compile all *.wasi files and store them to the
// returned map
func (b WasiBuilder) CompileModules(ctx context.Context, modules embed.FS) (map[string]Wasi, error) {
	ret := make(map[string]Wasi, 100)
	err := fs.WalkDir(wasmFiles, ".", func(path string, d fs.DirEntry, err error) error {
		if err != nil {
			return err
		}
		if d.IsDir() {
			return nil
		}
		if !strings.HasSuffix(d.Name(), ".wasm") {
			return nil
		}

		bytes, err := modules.ReadFile(path)
		if err != nil {
			return fmt.Errorf("read %s: %w", d.Name(), err)
		}
		mod, err := b.runtime.CompileModule(ctx, bytes)
		if err != nil {
			return fmt.Errorf("compiling wasi module %s: %w", d.Name(), err)
		}
		name := b.mapper.Name(path)
		ret[name] = b.Build(name, mod)

		return nil
	})
	return ret, err
}

// main writes an input file to stdout, just like `cat`.
//
// This is a basic introduction to the WebAssembly System Interface (WASI).
// See https://github.com/WebAssembly/WASI
func main() {
	ctx := context.Background()

	// Create a new WebAssembly Runtime.
	r := wazero.NewRuntime(ctx)
	defer r.Close(ctx) // This closes everything this Runtime created.

	// Instantiate WASI, which implements system I/O such as console output.
	wasi_snapshot_preview1.MustInstantiate(ctx, r)

	rooted, err := fs.Sub(catFS, "testdata")
	if err != nil {
		log.Fatalln(err)
	}
	config := wazero.NewModuleConfig().WithFS(rooted)

	builder := NewWasiBuilder(r, config)

	ret, err := builder.
		WithMapperFunc(func(path string) string {
			_, f := filepath.Split(path)
			return strings.TrimSuffix(f, filepath.Ext(f))
		}).CompileModules(ctx, wasmFiles)
	if err != nil {
		log.Fatalln(err)
	}

	stdio := unix.NewStdio(os.Stdin, os.Stdout, os.Stderr)
	err = ret["cat"].WithArgs("test.txt").Run(ctx, stdio)
	if err != nil {
		log.Fatalln(err)
	}
}
