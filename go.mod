module wexp

go 1.22rc2

require (
	codeberg.org/gonix/gio v0.0.0-20240112105409-28cf35822a8f
	github.com/stretchr/testify v1.8.1
	github.com/tetratelabs/wazero v1.6.0
)

require (
	github.com/TwiN/go-color v1.4.1
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
	gopkg.in/yaml.v3 v3.0.1 // indirect
	mvdan.cc/sh/v3 v3.7.0
)
