package main

import (
	"bytes"
	"context"
	"io"
	"io/fs"
	"log"
	"os"
	"path/filepath"
	"strings"
	"testing"
	"time"

	"codeberg.org/gonix/gio/unix"
	"github.com/stretchr/testify/require"
	"github.com/tetratelabs/wazero"
	"github.com/tetratelabs/wazero/imports/wasi_snapshot_preview1"
)

var utils map[string]Wasi

func TestMain(m *testing.M) {
	ctx := context.Background()

	// Create a new WebAssembly Runtime.
	r := wazero.NewRuntime(ctx)
	defer r.Close(ctx) // This closes everything this Runtime created.

	// Instantiate WASI, which implements system I/O such as console output.
	wasi_snapshot_preview1.MustInstantiate(ctx, r)

	rooted, err := fs.Sub(catFS, "testdata")
	if err != nil {
		log.Panicln(err)
	}
	config := wazero.NewModuleConfig().WithFS(rooted)

	builder := NewWasiBuilder(r, config)

	utils, err = builder.
		WithMapperFunc(func(path string) string {
			_, f := filepath.Split(path)
			return strings.TrimSuffix(f, filepath.Ext(f))
		}).CompileModules(ctx, wasmFiles)
	if err != nil {
		log.Fatalln(err)
	}
	os.Exit(m.Run())
}

func TestWasi(t *testing.T) {
	t.Parallel()

	eq := func(expected string) func(t *testing.T, actual string) {
		return func(t *testing.T, actual string) {
			t.Helper()
			require.Equal(t, expected, actual)
		}
	}
	ne := func(t *testing.T, actual string) {
		t.Helper()
		require.NotEmpty(t, actual)
	}

	var testCases = []struct {
		name   string
		args   []string
		stdout func(*testing.T, string)
		stderr func(*testing.T, string)
	}{
		{"basename", []string{"testdata/test.txt"}, eq("test.txt\n"), nil},
		{"cal", []string{"-1"}, ne, nil},
		{"cat", []string{"test.txt"}, eq("Hello web assembly and wasi_snapshot_preview1\n"), nil},
		{"cksum", []string{"test.txt"}, eq(" test.txt\n"), nil}, // this is what ./cksum form sbase prints
		{"comm", []string{"test.txt", "test.txt"}, eq("\t\tHello web assembly and wasi_snapshot_preview1\n"), nil},
		{"cp", []string{"test.txt", "test.txt"}, nil, eq("cp: test.txt -> test.txt: same file\n")},
		{"cut", []string{"-f", "1", "-d", " ", "test.txt"}, eq("Hello\n"), nil},
		{"date", []string{}, ne, nil},
		{"dd", []string{"-x"}, nil, eq("usage: dd [operand...]\n")},
		{"dirname", []string{"testdata/text.txt"}, eq("testdata\n"), nil},
		{"du", []string{"."}, eq("0\t.\n"), nil},
		{"echo", []string{"hello"}, eq("hello\n"), nil},
		{"expand", []string{"test.txt"}, eq("Hello web assembly and wasi_snapshot_preview1\n"), nil},
		{"expr", []string{"1", "+", "2"}, eq("3\n"), nil},
		{"false", []string{}, nil, eq("")},
		{"fold", []string{"test.txt"}, eq("Hello web assembly and wasi_snapshot_preview1\n"), nil},
		{"getconf", []string{}, nil, eq("usage: getconf [-v spec] var [path]\n")},
		{"grep", []string{"wasi", "test.txt"}, eq("Hello web assembly and wasi_snapshot_preview1\n"), nil},
		{"head", []string{"test.txt"}, eq("Hello web assembly and wasi_snapshot_preview1\n"), nil},
		{"join", []string{"test.txt", "test.txt"}, eq("Hello web assembly and wasi_snapshot_preview1 web assembly and wasi_snapshot_preview1\n"), nil},
		{"link", []string{}, nil, eq("usage: link target name\n")},
		{"ln", []string{}, nil, ne},
		{"logname", []string{}, nil, eq("logname: no login name\n")},
		{"md5sum", []string{"test.txt"}, eq("21c1a27afdcaef9a81ad5de75d43637d  test.txt\n"), nil},
		{"mkdir", []string{}, nil, eq("usage: mkdir [-p] [-m mode] name ...\n")},
		{"mv", []string{}, nil, eq("usage: mv [-f] source ... dest\n")},
		{"nl", []string{"test.txt"}, eq("     1\tHello web assembly and wasi_snapshot_preview1\n"), nil},
		{"od", []string{"test.txt"}, eq("0000000     15433062510     14535620157     16330220142     14233262563\n0000020     14110074554     16710062156     13732271541     16030267163\n0000040     16433664163     14534470137     16731264566            5061\n0000056\n"), nil},
		{"paste", []string{"test.txt", "test.txt"}, eq("Hello web assembly and wasi_snapshot_preview1\tHello web assembly and wasi_snapshot_preview1\n"), nil},
		{"pathchk", []string{"test.txt"}, eq(""), nil},
		{"printenv", []string{}, eq(""), nil},
		{"printf", []string{"Hello"}, eq("Hello"), nil},
		{"pwd", []string{}, eq("/\n"), nil},
		{"readlink", []string{"test.txt"}, nil, eq("readlink: readlink test.txt: Function not implemented\n")},
		{"rev", []string{"test.txt"}, eq("1weiverp_tohspans_isaw dna ylbmessa bew olleH\n"), nil},
		{"rm", []string{}, nil, eq("usage: rm [-f] [-Rr] file ...\n")},
		{"rmdir", []string{}, nil, eq("usage: rmdir [-p] dir ...\n")},
		{"sed", []string{"s/wasi/WASI/", "test.txt"}, eq("Hello web assembly and WASI_snapshot_preview1\n"), nil},
		{"seq", []string{"40", "42"}, eq("40\n41\n42\n"), nil},
		{"sha1sum", []string{"test.txt"}, eq("e38d1a5d2d6bce7c8f4397dfc3d1e668186100af  test.txt\n"), nil},
		// TODO: shaXYsum
		{"sleep", []string{}, nil, eq("usage: sleep num\n")},
		{"sort", []string{"test.txt"}, eq("Hello web assembly and wasi_snapshot_preview1\n"), nil},
		{"split", []string{"-h"}, nil, eq("usage: split [-a num] [-b num[k|m|g] | -l num] [-d] [file [prefix]]\n")},
		{"strings", []string{"test.txt"}, eq("Hello web assembly and wasi_snapshot_preview1\n"), nil},
		{"tail", []string{"test.txt"}, eq("Hello web assembly and wasi_snapshot_preview1\n"), nil},
		{"test", []string{"1", "-eq", "1"}, eq(""), nil},
		{"touch", []string{"test.txt"}, nil, eq("touch: utimensat test.txt: Function not implemented\n")},
		{"tr", []string{}, nil, eq("usage: tr [-cCds] set1 [set2]\n")},
		{"true", []string{}, eq(""), nil},
		{"uname", []string{}, eq("wasi\n"), nil},
		{"unexpand", []string{"-a", "test.txt"}, eq("Hello web assembly and wasi_snapshot_preview1\n"), nil},
		{"uniq", []string{"test.txt"}, eq("Hello web assembly and wasi_snapshot_preview1\n"), nil},
		{"unlink", []string{}, nil, eq("usage: unlink file\n")},
		{"uudecode", []string{"test.txt"}, nil, eq("uudecode: web: invalid mode\n")},
		{"uuencode", []string{"test.txt"}, ne, nil},
		{"wc", []string{"test.txt"}, eq("1 5 46 test.txt\n"), nil},
		{"which", []string{}, nil, eq("usage: which [-a] name ...\n")},
	}

	for _, tt := range testCases {
		tt := tt
		t.Run(tt.name, func(t *testing.T) {
			t.Parallel()
			var stdout bytes.Buffer
			var stderr bytes.Buffer

			stdio := unix.NewStdio(nil, &stdout, io.MultiWriter(os.Stderr, &stderr))
			err := utils[tt.name].WithArgs(tt.args...).Run(context.Background(), stdio)

			if tt.stdout != nil {
				require.NoError(t, err)
				tt.stdout(t, stdout.String())
			}

			if tt.stderr != nil {
				require.Error(t, err)
				tt.stderr(t, stderr.String())
			}
		})
	}
}

func TestYes(t *testing.T) {
	ctx, cancel := context.WithTimeout(context.Background(), 1*time.Millisecond)
	t.Cleanup(cancel)

	var stdout bytes.Buffer
	stdio := unix.NewStdio(nil, &stdout, os.Stderr)

	go func(t *testing.T) {
		t.Logf("START: yes")
		err := utils["yes"].Run(ctx, stdio)
		t.Logf("END: yes")
		require.NoError(t, err)
	}(t)

	<-ctx.Done()
	require.Equal(t, "y\ny\ny\n", stdout.String()[:6])
}
