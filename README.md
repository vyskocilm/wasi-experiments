# wasi experiments

Join https://codeberg.org/gonix/gio with WebAssembly and WASI preview1

* `testdata/*.wasm` is a cat from https://core.suckless.org/sbase/ project build to wasm via was-sdk 20.0
* `main.go` is an experiment in how to implement a compatible and flexible `gonix/gio` compatible wrapper running sbase code using web assembly
* `main_test.go` implements a smoke tests for all of utilities

See https://git.suckless.org/sbase/file/LICENSE.html for a LICENSE of sbase program.

## Usage

```sh
$ go run main.go test.txt
Hello web assembly and wasi_snapshot_preview1
```

```sh
$ go test -v ./...
```

# WIP

## use mvdan/sh as an experiment

See `sh_test.go` the test dealing with pipes and simple colons

 1. parsing done
 2. WIP: code parsing sh and generating the Go counterpart, which will execute it
 3. WIP: look at https://pkg.go.dev/mvdan.cc/sh/v3/interp


# TODO

 * how to cancel running wasi code? - the TestYes works around it by calling the yes in a goroutine
 * review printenv - aka how to pass env variabled to wasm code
 * review unimplemented stuff and how API can be extended to use that
 * how about shell builtins like until, if, ...?
